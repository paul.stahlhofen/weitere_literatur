The authors review two approaches which they proposed themselves for the
BatLeDIM challenge and which scored third place. Both approaches are used for
leak localization (not leak detection) and assume that one already knows
that a leak is present. For the competition, they used the data-driven
approach in area A and the model-based approach in areas B and C. This was
done based on availability of pressure measurements (important for
data-driven) and availability of AMR (demand sensors, important for
model-based.)

One approach is data-driven, requiring the network structure and readings from
pressure sensors. The other approach is model-based and uses pressure and
demand information, as well as a calibrated model of the network.

Data-driven approach
The authors use a linear approximation to the Hazen-Williams equation for
state estimation.  The Hazen-Williams equation is an empirically determined
and easy to use equation relating the velocity of water to geometric
properties of the pipe.  In particular, it can be used to relate flow to head
loss and thereby to pressure. This is the central equation (probably also used
in EPANET) which is used to determine pressures given flows. There are other
alternatives to the Hazen-Williams equation which may have advantages in other
circumstances. For example, the Hazen-Williams equation only works for water
with standard temparature and viscosity, so it is not very general.
The authors approximate the relation between different heads by a formula that
looks like a one-layer GNN with the weights fixed to inverse lengths of pipes
between neighbours. The authors impose additional constraints for
directionality and one constraint that enforces the pressure heads at nodes
with sensors to equal the measured values. This state estimation is performed
for a leak-free and a leaky scenario to produce vectors h_nom and h_leaky,
respectively. The estimated pressure values are than used to generate a cloud
of points where the values of h_nom are used as x-coordinates and the values
of h_leaky as y-coordinates. A linear regression model is fit to the point
cloud. The point for which the prediction of the regression model has the
largest error is taken as the most likely leak candidate. Only errors where
the regression estimated a higher pressure than given by the point are taken
into account. In essence, this computes the point with the largest pressure
drop. The standard deviation of the regression model's error is then used to
determine a threshold for the leak candidate set.

For the model-based approach, it is important to estimate the demands which
are then fed into EPANET. For this purpose, the authors used automated metered
readings (AMRs) from sensors installed in EPANET, as well as the inlet flow.
Once demands are given, N simulations (for each node with a leak placed there)
are run in EPANET. The node for which the simulated leak produced pressure
values that are closest to the actual measurements is picked as the most
likely candidate. The leak magnitude needed for the simulation is estimated by
the total demand of the WDN (inlet flow).

In conclusion, the data-driven approach performs better when pressure sensors
are available while the model-based approach performs better when demand
sensors are available. The model-based approach seems to be more successful in
general, but it also requires a well calibrated hydraulic model of the
network.
